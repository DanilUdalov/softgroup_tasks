class Task1
  def initialize
    @array = [13, 8, 15, 12, 6, 10, 4, 13, 16, 10]
    @x = 20
  end

  def minimum_of_elements
    sum = 0
    @x = gets.to_i
    @array.sort.reverse.each_with_index do |e, i|
      puts "element #{e}"
      sum += e
      return i + 1 if sum >= @x
    end
    return nil
  end
end
