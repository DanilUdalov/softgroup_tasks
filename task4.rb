class Task4
  def sum
    s = (1..100).inject { |sum, n| sum + n }
    "Sum all of the numbers 1 and 100 = #{s}"
  end
end
