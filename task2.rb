class Task2
  def initialize
    @array = [13, 11, 23, 26, 1, 15, 12, 6, 18, 7, 13, 16, 10]
  end

  def index_array
    a = gets.to_i
    @array_sort = @array.sort
    if @array_sort.index(a)
      "Index number #{a} = #{@array_sort.index(a)}"
    else
      'This number is not present in the array'
    end
  end
end
