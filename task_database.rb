require 'sequel'

DB = Sequel.sqlite

DB.create_table :users do
  primary_key :id
  String :name
  String :email
  DateTime :birthday
end

DB.create_table :posts do
  primary_key :post_id
  foreign_key :user_id, :users
  String :title
  Text :body
end

users = DB[:users]
posts = DB[:posts]

dan_id = users.insert(name: 'Dan', email: 'dan@mail.ru', birthday: '1990-01-01')
ban_id = users.insert(name: 'Ban', email: 'ban@mail.ru', birthday: '2001-04-04')
maggy_id = users.insert(name: 'Maggy', email: 'maggy@mail.ru', birthday: '1995-01-01')

posts.insert(user_id: dan_id, title: 'First post', body: 'This is a first post')
posts.insert(user_id: ban_id, title: 'Second post', body: 'This is a second post')
posts.insert(user_id: maggy_id, title: 'Three post', body: 'This is a three post')
posts.insert(user_id: maggy_id, title: 'Four post', body: 'This is a four post')
posts.insert(user_id: ban_id, title: 'Five post', body: 'This is a five post')
posts.insert(user_id: maggy_id, title: 'Six post', body: 'This is a six post')

puts '=====================All users====================='
users.each do |row|
  p row
end

puts '=====================All posts====================='
posts.each do |row|
  p row
end

puts '==================================================='
# Query to get all users older than 18 years
puts users.where { birthday < (Date.today - 6574.5) }.all
puts '==================================================='
# Query to get all users with their posts
puts users.graph(:posts, user_id: :id).select(:name, :id, :title, :body).all
